package server.side;


import server.side.Helper.WrapperList;
import server.side.models.Box;
import server.side.models.CharacterObj;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


public class GameServer {
	private static String TILES_FILE;
	//refreshing game state and sending data to clients every x ms
	private static final long RESHRESH_GAP = 30;
	private static int SERVER_PORT_TCP;
	private static long IDs = 0L;
	
	//thread safe array because while one thread is reading another
	//might add delete some entries
	private CopyOnWriteArrayList<IpPort> activeClients;
	private Vector<MainCharacter> fullCharacters;

	private WrapperList tiles;
	private WrapperList gamePlay;
	
	private UdpConnectionsSender udpSender;

	public static void main(String[] args) {
		GameServer main = new GameServer();
		System.out.println("Server started properly. Waiting for clients.");
		main.serverStart();
	}
	
	public GameServer(){
		readServerConfig();
		activeClients = new CopyOnWriteArrayList<IpPort>();
		tiles = new WrapperList();
		gamePlay = new WrapperList();
		udpSender = new UdpConnectionsSender();
		fullCharacters = new Vector<MainCharacter>();
	}
	
	private static void readServerConfig(){
		File file = new File("src/server/side/settings/serverConfiguration.xml");
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder;
		try {
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document;
			document = documentBuilder.parse(file);
			String tcpPort = document.getElementsByTagName("tcpPort").item(0).getTextContent();
			SERVER_PORT_TCP = Integer.parseInt(tcpPort);
			String gameTiles = document.getElementsByTagName("gameTiles").item(0).getTextContent();
			TILES_FILE = gameTiles;
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void serverStart(){
		gameStateRefresher();
		
		InputStream ff = GameServer.class.getResourceAsStream(TILES_FILE);
		
		try (Scanner fileReader = new Scanner(ff);
		ServerSocket serverSocket = new ServerSocket(SERVER_PORT_TCP)){
			while(fileReader.hasNext()){
				tiles.add(new Box(fileReader.nextInt(), fileReader.nextInt(),
						fileReader.nextInt(), fileReader.nextInt(), -1L, -1, 0));
			}
			
			Socket clientSocket;
			while((clientSocket = serverSocket.accept()) != null){
				new Thread(new TcpConnection(this, clientSocket)).start();
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void gameStateRefresher(){
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				updateGamePlay();
				udpSender.sendGamePlay();
			}
			
			private void updateGamePlay() {
				gamePlay.clear();
				for (MainCharacter mc : fullCharacters){
					gamePlay.addAll(mc.update(tiles.realList, fullCharacters));
				}
			}
			
		}, 0, RESHRESH_GAP);
	}
	
	public synchronized long getId(){
		return IDs++;
	}
	
	/**
	 * This function is called when new data about character arrives.
	 * If this is a new character we update its state otherwise
	 * we simply update velocity and etc.
	 * @param data that we get from client
	 */
	
	public void includeCharacter(CharacterObj data){
		long specId = data.id;
		for (MainCharacter mc : fullCharacters){
			//if character already exists we just update its status
			if (specId == mc.getID()){
			 	mc.updateState(data);
				return ;
			}
		}
		//if it is new character then we add it to the list
		MainCharacter newMc = new MainCharacter(data);
		fullCharacters.add(newMc);
	}
	
	public void removeCharacter(long id){
		Iterator<MainCharacter> i = fullCharacters.iterator();
		while(i.hasNext()){
			
			MainCharacter mc = i.next();
			if (mc.getID() == id){
				i.remove();
				return;
			}
		}
	}
	

	public void addressBook(InetAddress address, int port){
		activeClients.add(new IpPort(address, port));
	}
	
	public static class IpPort{
		InetAddress address;
		int port;
		
		public IpPort(InetAddress address, int port){	
			this.address = address;
			this.port = port;
		}
	} 
	
	public WrapperList getMap(){
		return tiles;
	}
	
	private class UdpConnectionsSender {
		DatagramSocket gamePlaySocket;
		
		public UdpConnectionsSender() {
			try {
				gamePlaySocket = new DatagramSocket();
			} catch (SocketException e) {
				e.printStackTrace();
			}
		}
		
		public void sendGamePlay() {
			try{
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
				objectOutputStream.writeObject(Helper.marshall(gamePlay));
				byte [] bytes = byteArrayOutputStream.toByteArray();
				DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
				
				/*send gameplay to every active client*/
				for (IpPort destination : activeClients){
					packet.setAddress(destination.address);
					packet.setPort(destination.port);
					gamePlaySocket.send(packet);
					packet.setData(bytes);
					packet.setLength(bytes.length);
				}
				
			}catch (IOException | JAXBException e) {
				e.printStackTrace();
			}
		}
	}
}