package server.side.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Bullet implements Serializable{
	private static final long serialVersionUID = -1951636015504203979L;
	
	public float x;
	public float y;
	
	public float k;
	public float c;
	public float pn;
	
	public String direction;
	
	public Bullet(){}

	
	public Bullet(float x, float y, String direction){
		this.x = x;
		this.y = y;
		this.direction = direction;
	}
}
