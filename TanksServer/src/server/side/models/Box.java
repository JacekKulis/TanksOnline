package server.side.models;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Box implements Serializable{
	private static final long serialVersionUID = -2240347093423788662L;
	
	public float x;	//position
	public float y;
	
	public int w;	//widht
	public int h;	//height
	
	public int rot;
	
	public int xp;	//health
	
	public long id;	//id
	
	public Box(){
		
	}
	
	public Box(float x, float y, int width, int height, long id, int xp, int rot) {
		this.x = x;
		this.y = y;
		this.w = width;
		this.h = height;
		
		this.id= id;
		this.xp = xp;
		
		this.rot = rot;
	}

	
}