package client.side;

import java.io.StringReader;

import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import client.side.models.Box;
import client.side.models.ServerMessage;

/**
 * Helper class for changing data to xml and in the opposite
 */

public class Helper {
	public static String marshall(ServerMessage sm) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(ServerMessage.class);
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		
		StringWriter sw = new StringWriter();
		marshaller.marshal(sm, sw);
		
		return sw.toString();
	}
	
	public static List<Box> unmarshall(String data) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance(WrapperList.class);
		
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		StringReader sr = new StringReader(data);
		
		WrapperList wrapList = (WrapperList) unmarshaller.unmarshal(sr);
		return wrapList.realList;
	}
	
	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class WrapperList{
		
		List<Box> realList;
		public WrapperList(){}
		
	}

}
