package client.side.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ServerMessage{
	
	public CharacterObj characterData;

	public int messageType;
	public long id;
	public int port;
	
	public ServerMessage(){}
	
	public ServerMessage(int msgType){
		messageType = msgType;
	}
	
	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	public long getId() {
		return id;
	}

	public int getPort() {
		return port;
	}
	
	public void setCharacterData(CharacterObj data){
		characterData = data;
	}
	
	public CharacterObj getCharacterData() {
		return characterData;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
}
