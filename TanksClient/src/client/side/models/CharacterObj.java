package client.side.models;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CharacterObj{
	public int xVel;
	public int yVel; 
	
	public int rot;
	
	public long id;
	
	public List<Bullet> newBullets;
	
	public CharacterObj(){}
	
	public CharacterObj(int xVel, int yVel, int rot, long id) {
		this.xVel = xVel;
		this.yVel = yVel;
		this.rot = rot;
		this.id = id;
	}
	
	public int getxVel() {
		return xVel;
	}

	public void setxVel(int xVel) {
		this.xVel = xVel;
	}

	public int getyVel() {
		return yVel;
	}

	public void setyVel(int yVel) {
		this.yVel = yVel;
	}

	public int getRot() {
		return rot;
	}

	public void setRot(int rot) {
		this.rot = rot;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Bullet> getNewBullets() {
		return newBullets;
	}

	public void setNewBullets(List<Bullet> newBullets) {
		this.newBullets = newBullets;
	}

}
