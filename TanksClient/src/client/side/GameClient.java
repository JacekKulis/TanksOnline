package client.side;

import static org.lwjgl.opengl.GL11.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;
import client.side.models.Box;
import client.side.models.Bullet;
import client.side.models.CharacterObj;

public class GameClient {
	/** */
	private static final int DISPLAY_WIDTH = 700;
	private static final int DISPLAY_HEIGTH = 700;
	
	private static final int FRAMES_PER_SECOND = 30;
	
	static long ID = -1; // we get ID from the server side
	
	private TcpConnection connections; // establishing TCP connection
	
	private CharacterObj character; // data about the main character
	private List<Bullet> bullets; // bullets shot in every frame, also to server

	private List<Box> obstacles;
	private List<Box> movingObjects; // all players and bullets. We get this from server
	private Box updatedCharacter; // clients character that we get from server

	private String serverIP;
	private int serverPortTCP;
	private int clientPortUDP;
	
	private Texture obstacleTexture;
	private Texture tankTexture;
	
	/** Event varbiables*/
	private boolean up = false;
	private boolean down = false;
	private boolean right = false;
	private boolean left = false;
	private boolean readyToShoot = false;
	
	
	public void updateReadyToShoot() {
		readyToShoot = true;
	}
	
	public GameClient(String ip, int portTCP, int portUDP){
		serverIP = ip;
		serverPortTCP = portTCP;
		clientPortUDP = portUDP;
	}
	
	public static void main(String[] args) {
		if (args.length != 3){
			throw new IllegalArgumentException("Bad input. You need [IP] [TCP PORT] [UDP PORT]");
		}
		
		GameClient main = new GameClient(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]));
		main.initOpenGl();
		main.init();
		main.start();
	}
	
	/** Initializing OpenGL functions */
	private void initOpenGl() {
		try {
			Display.setDisplayMode(new DisplayMode(DISPLAY_WIDTH, DISPLAY_HEIGTH));
			Display.setResizable(false);
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		glMatrixMode(GL_PROJECTION);
		glEnable(GL_TEXTURE_2D);
		//glEnable(GL_BLEND);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		glLoadIdentity();
		glOrtho(0, DISPLAY_WIDTH, DISPLAY_HEIGTH, 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
	}
	
	private void init() {
		connections = new TcpConnection(this, serverIP, serverPortTCP);
		
		if ((ID = connections.getIdFromServer()) == -1) {
			System.err.println("cant get id for char");
		}
		
		obstacles = connections.getMapDetails();
		character = new CharacterObj(0, 0, 270, ID);
		bullets = new ArrayList<Bullet>();
		movingObjects = new ArrayList<Box>();
		
		new Thread(new UdpConnection(this, connections, clientPortUDP)).start();
		
		final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
		executorService.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				updateReadyToShoot();
			}
		}, 0, 2, TimeUnit.SECONDS);
	}
	
	/** Game loop */
	private void start() {
		try {
			obstacleTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("src/client/res/obstacle/obstacle.png"));
			tankTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("src/client/res/tank/tank.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		while (!Display.isCloseRequested()) {
			glClear(GL_COLOR_BUFFER_BIT);
			if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
				closingOperations();
			}
			
			
			handlingEvents();
			sendCharacter();
			render();
			Display.update();
			Display.sync(FRAMES_PER_SECOND);
			glGetError();
		}
		closingOperations();
	}

	
	/** Rendering obstacles, players and bullets */
	private void render() {
		for (Box box : obstacles) {
			drawSquareObstacles(box, obstacleTexture);
		}
		
		for (Box box : movingObjects) {
			drawSquarePlayer(box, tankTexture);
		}
	}
	
	
	/** Function to draw obstacles  */
	private void drawSquareObstacles(Box box, Texture texture) {
		glBindTexture(GL_TEXTURE_2D, texture.getTextureID());
		glLoadIdentity();
		
		glBegin(GL_QUADS);
			glVertex2f(box.x, box.y);
			glTexCoord2f(0,0);
			
			glVertex2f(box.x + box.w, box.y);
			glTexCoord2f(1,0);
			
			glVertex2f(box.x + box.w, box.y + box.h);
			glTexCoord2f(1,1);
			
			glVertex2f(box.x, box.y + box.h);
			glTexCoord2f(0,1);
		glEnd();
	}
	
	/** Function to draw players  */
	private void drawSquarePlayer(Box box, Texture texture) {
		glBindTexture(GL_TEXTURE_2D, texture.getTextureID());
		
		glLoadIdentity();
		glTranslatef(box.x+box.w/2, box.y+box.h/2, 0);
		glRotatef(box.rot, 0, 0, 1);
		glTranslatef(-(box.x+box.w/2), -(box.y+box.h/2), 0);
		
		glBegin(GL_QUADS);
			glVertex2f(box.x, box.y);
			glTexCoord2f(0,0);
			
			glVertex2f(box.x + box.w, box.y);
			glTexCoord2f(1,0);
			
			glVertex2f(box.x + box.w, box.y + box.h);
			glTexCoord2f(1,1);
			
			glVertex2f(box.x, box.y + box.h);
			glTexCoord2f(0,1);
		glEnd();
		
		glPopMatrix();
	}
	
	/** Function to send main characters data to server */
	private void sendCharacter() {
		character.newBullets = bullets;
		connections.sendUpdatedVersion(character);
		bullets.clear();
	}

	/** Closing game */
	private void closingOperations() {
		connections.removeCharacter(ID);
		Display.destroy();
		System.exit(0);
	}

	void updateListOfObjects(List<Box> objects) {
		if (objects == null)	return;
		movingObjects = objects;
		for (Box box : objects) {
			if (box.id == ID) {
				updatedCharacter = box;
				break;
			}
		}
	}

	private void handlingEvents() {
		if (Display.isActive()) {
			
			while (Keyboard.next()) {
				if (Keyboard.getEventKey() == Keyboard.KEY_SPACE && updatedCharacter != null){
					float xmain = updatedCharacter.x + updatedCharacter.w / 2;
					float ymain = updatedCharacter.y + updatedCharacter.h / 2;
					
					if (character.getRot() == 180 && readyToShoot){
						bullets.add(new Bullet(xmain, ymain, "up"));
						readyToShoot = false;
					}
					else if (character.getRot() == 0 && readyToShoot){
						bullets.add(new Bullet(xmain, ymain, "down"));
						readyToShoot = false;
					}
					else if (character.getRot() == 270 && readyToShoot){
						bullets.add(new Bullet(xmain, ymain, "right"));
						readyToShoot = false;
					}
					else if (character.getRot() == 90 && readyToShoot){
						bullets.add(new Bullet(xmain, ymain, "left"));
						readyToShoot = false;
					}
					
					
				}
				
				if (Keyboard.getEventKey() == Keyboard.KEY_UP && !left && !right) {
					if (Keyboard.getEventKeyState()) {
						character.yVel = -5;
						character.setRot(180);
						up = true;
					} else {
						up = false;
						if (!down) {
							character.yVel = 0;
						}
					}
				}
				else if (Keyboard.getEventKey() == Keyboard.KEY_DOWN && !left && !right) {
					if (Keyboard.getEventKeyState()) {
						character.yVel = 5;
						character.setRot(0);
						down = true;
					} else {
						down = false;
						if (!up) {
							character.yVel = 0;
						}
					}
				}
				else if (Keyboard.getEventKey() == Keyboard.KEY_RIGHT && !up && !down) {
					if (Keyboard.getEventKeyState()) {
						character.xVel = 5;
						character.setRot(270);
						right = true;
					} else {
						right = false;
						if (!left) {
							character.xVel = 0;
						}
					}
				}
				else if (Keyboard.getEventKey() == Keyboard.KEY_LEFT && !up && !down) {
					if (Keyboard.getEventKeyState()) {
						character.xVel = -5;
						character.setRot(90);
						left = true;
					} else {
						left = false;
						if (!right) {
							character.xVel = 0;
						}
					}
				}
			}
		} else {
			character.xVel = 0;
			character.yVel = 0;
		}
	}
}